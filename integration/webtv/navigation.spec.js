/// <reference types="cypress" />

// import { history } from '../../../src/modules/routing/utils/history';

context('Navigation', () => {
  beforeEach(() => {
    cy.initStore();
    cy.bypassPrivacyPolicy();
  });

  describe('L1', () => {
    it('changes the route', () => {
      cy.pause();
      // console.warn(history.location);
    });
  });

  describe('Live TV', () => {
    /** @see https://plutotv.testrail.net/index.php?/cases/view/14404 */
    /** @see https://plutotv.testrail.net/index.php?/cases/view/58924 */
    /** @see https://plutotv.testrail.net/index.php?/cases/view/58925 */
    describe('L2', () => {
      it('changes the category');
    });

    /** @see https://plutotv.testrail.net/index.php?/cases/view/58926 */
    describe('L3', () => {
      it('changes the content');
    });

    describe('L4', () => {
      it('...');
    });
  });

  describe('VOD', () => {
    describe('L2', () => {
      it('changes the category');
    });

    describe('L3', () => {
      it('changes the content');
    });

    describe('L4', () => {
      it('...');
    });
  });
});
