/// <reference types="cypress" />

/** @see https://www.cypress.io/blog/2018/11/14/testing-redux-store/ */
context('Store', () => {
  before(function () {
    cy.fixture('store.json').then((store) => (this.store = store));
    cy.initStore();
    cy.bypassPrivacyPolicy();
  });

  it('initial state', function () {
    cy.getState().then((state) => {
      expect(state).to.deep.equal(this.store);
    });
  });
});
