/// <reference types="cypress" />

import { LiveTVRoutes, BaseRoutes } from '../../../src/modules/routing/constants';

const CONFIG = require('../../fixtures/CONFIG');
const translation = require('../../../src/shared/assets/i18n/en/translation');

const privacyPolicyEnabled = CONFIG.privacyPolicy.enabled && CONFIG.privacyPolicy.show;

/** @see https://plutotv.testrail.net/index.php?/cases/view/14401 */
context('App Launch', () => {
  before(() => cy.visit('/'));

  /**
   * intercepts AND fixtures are cleaned up after EVERY test, see https://github.com/cypress-io/cypress/discussions/9395
   * must specify GET to ignore OPTIONS
   */
  beforeEach(() => {
    cy.intercept('GET', CONFIG.bootstrap.api.baseUrl).as('boot'); // { fixture: 'bootstrap.json }
    cy.intercept('GET', 'guide').as('guide');
    cy.intercept('master.m3u8').as('master');
  });

  it('Splash Loader', () => {
    /** Root element */
    cy.get('#plutotv-app')
      .as('app')
      .children('[data-testid="splash-loader"]')
      .as('splash')
      .should('be.visible')
      .and('contain', translation['drop_in_its_free']);
  });

  describe('First Launch', () => {
    (privacyPolicyEnabled ? it : it.skip)('Privacy Policy', () => {
      // this.timeout(60000); // CONFIG.liveTV.hideGuideDelay

      // cy.get('@CONFIG', { log: false }).then(cy.log);

      /** the app automatically redirects */
      cy.location('pathname', { log: false }).should('eq', BaseRoutes.privacyPolicy);
      expect(localStorage.getItem('privacyPolicyVersion')).to.be.null;

      cy.get('[data-testid="privacy-policy-bar-title"]')
        .as('policy')
        .should('be.visible')
        .and('contain', translation['by_clicking_accept_you_agree']);
      cy.get('[data-testid="privacy-policy-bar-review"]')
        .as('link-to-full')
        .should('be.visible')
        .and('contain', translation['review_update']);
      cy.get('[data-testid="privacy-policy-bar-accept"]')
        .as('accept-button')
        .should('be.visible')
        .and('contain', translation['accept_and_continue'])

        /** Accept */
        .click()
        .should(() => {
          expect(localStorage.getItem('privacyPolicyVersion')).to.equal(
            CONFIG.privacyPolicy.version,
          );
        });

      cy.wait('@guide').its('response.statusCode').should('be.oneOf', [200, 304]);

      /** Redirects to Live TV */
      cy.location('pathname', { log: false }).should('eq', LiveTVRoutes.guide);

      cy.get('@policy').should('not.exist');
      cy.get('@link-to-full').should('not.exist');
      cy.get('@accept-button').should('not.exist');
    });
  });

  it('video autoplay', () => {
    // cy.get('@CONFIG', { log: false }).then(cy.log);

    cy.wait(['@boot', '@master']).spread((boot, master) => {
      const startingChannel = boot.response.body.startingChannel;

      Cypress.log({
        name: 'startingChannel',
        displayName: 'Starting Channel',
        message: [startingChannel.slug, `id: ${startingChannel.id}`],
      });
      expect(master.request.url).to.include(startingChannel.id); // guide.response.body.channels[0].stitched.urls[0].url
    });

    cy.get('#video', { log: false }).as('player').should(
      'have.attr',
      'src',
      // `blob:http://dev.pluto.tv:5000/d9f9223f-2b87-4091-8377-0a56a700564a`,
    );
  });
});
