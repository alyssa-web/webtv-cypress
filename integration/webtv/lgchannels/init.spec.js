/// <reference types="cypress" />

context('LG Channels', () => {
  before(() => {
    // cy.intercept('https://qt2-aic.cdpsvc.lgtvcommon.com/lgchannels/lgchannels.livedmost.0.0.29.min.js').as('sdk');
    cy.visit('/channels.html');
  });

  describe('init', () => {
    it('loads SDK', () => {
      cy.window().its('lgchannels').should('exist');
      cy.window().its('lgchplus').should('exist');
    });
    it('initializes Video Player', () => {
      cy.window().its('VideoPlayer').should('exist');
    });
  });

  describe('custom events', () => {
    it('channelChange', () => {
      cy.document().then((doc) => {
        doc.dispatchEvent(
          new CustomEvent('channelChange', {
            detail: { channelId: '5ca525b650be2571e3943c63', lock: false },
          }),
        );
      });
    });

    it('systemLock');

    it('getLimitAdTracking');

    it('getDoNotSellMyPersonalInformation');

    it('updatePreferenceRating');

    it('updateClosedCaptionOption');

    it('chplusStatus');
  });
});
