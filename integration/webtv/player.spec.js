/// <reference types="cypress" />

import { generatePath } from 'react-router-dom';

import { LiveTVRoutes, BaseRoutes } from '../../../src/modules/routing/constants';

context('Player', () => {
  beforeEach(() => {
    cy.initStore();
    cy.bypassPrivacyPolicy();
  });

  describe('Closed Captions', () => {
    let channelSlug, prevChannelSlug;

    it('enable', () => {
      cy.location('pathname', { log: false }).should('equal', LiveTVRoutes.guide);

      cy.getStartingChannel().then(({ slug }) => {
        channelSlug = slug;
      });
      cy.onBack().then(() => {
        cy.log('Close Guide');
      });

      cy.location().should((location) => {
        expect(location.pathname).to.not.eq(LiveTVRoutes.guide);
        expect(location.pathname).to.eq(generatePath(LiveTVRoutes.watch, { slug: channelSlug }));
      });
      
      cy.pause();
      cy.getState().its('player.ccEnabled').should('be.true');
    });
    it('disable', () => {
      cy.getState().its('player.ccEnabled').should('be.false');
      cy.pause();
    });
  });
});
