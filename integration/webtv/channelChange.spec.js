/// <reference types="cypress" />

import { generatePath } from 'react-router-dom';

// import { userInput } from '../../../src/modules/user-input/redux/userInput.duck';
// import { selectPlayerContentSlug } from '../../../src/modules/player/redux/state/content.duck';
// import { liveTVEPGSelectedIndexSelector } from '../../../src/modules/live-tv/redux/selectors/epg/selectedIndex';

import { LiveTVRoutes, BaseRoutes } from '../../../src/modules/routing/constants';

const dPad = (direction) => {
  /** this shows up in the command log at the top and out of order */
  // Cypress.log({
  //   name: 'dpad',
  //   displayName: 'D-PAD',
  //   message: direction,
  // });

  // cy.document().trigger('keyDown', { eventConstructor: 'KeyboardEvent', keyCode: 38 });
  // cy.document().trigger('keyUp', { eventConstructor: 'KeyboardEvent', keyCode: 38 });
  // cy.get('body').type('{upArrow}');
  cy.get('@fakeDocument', { log: false })
    .then((document) => {
      // log must be here or else it shows up out of order
      Cypress.log({
        name: 'dpad',
        displayName: 'D-PAD',
        message: direction,
      });

      return document;
    })
    .type({ up: '{upArrow}', down: '{downArrow}' }[direction], {
      force: true,
      log: false,
    });
};

const rocker = (direction) => {
  /** this shows up in the command log at the top and out of order */
  // Cypress.log({
  //   name: 'rocker',
  //   displayName: 'Channel Rocker',
  //   message: direction,
  // });

  // cy.document().trigger('keyDown', { eventConstructor: 'KeyboardEvent', keyCode: 38 });
  // cy.document().trigger('keyUp', { eventConstructor: 'KeyboardEvent', keyCode: 38 });
  // cy.get('body').type('{upArrow}');
  cy.get('@fakeDocument', { log: false })
    .then((document) => {
      // log must be here or else it shows up out of order
      Cypress.log({
        name: 'rocker',
        displayName: 'Channel Rocker',
        message: direction,
      });

      return document;
    })
    .type({ up: 'k', down: 'j' }[direction], {
      force: true,
      log: false,
    });
};

const guide = () => {
  cy.get('#plutotv-app').as('app').find('[data-testid="guide"]').as('guide').should('be.visible');
};

const openEPG = () => {
  cy.log('Open EPG');
  cy.location('pathname', { log: false }).should('eq', LiveTVRoutes.guide);

  // cy.dispatch(userInput.actions.keyPress({ code: 'onOk' })); // { type: '@@userInput/USER_INPUT/KEY_PRESS', payload: { code: 'onOk' } }
  cy.get('body', { log: false }).as('document').type('{enter}', { log: false });
  guide();

  cy.get('a', { log: false }).first({ log: false }).as('fakeDocument');

  // activate L2/L3
  dPad('down');

  cy.getState().then((state) => {
    // liveTVEPGSelectedIndexSelector(state);

    Cypress.log({
      name: 'selectedIndex',
      displayName: 'EPG Selected Index',
      message: state.liveTV.epg.selectedIndex,
      consoleProps: () => ({
        epg: state.liveTV.epg,
      }),
    });
    expect(state.liveTV.epg.selectedIndex).to.eq(1);
  });
};

/** @see https://www.cypress.io/blog/2018/11/14/testing-redux-store/ */
context('Channel Change', () => {
  let channelSlug, prevChannelSlug;

  beforeEach(() => {
    cy.initStore();
    cy.bypassPrivacyPolicy();
  });

  describe('Fullscreen', () => {
    /** @see https://plutotv.testrail.net/index.php?/cases/view/14402 */
    it('D-PAD UP/DOWN', () => {
      cy.location('pathname').should('eq', LiveTVRoutes.guide);

      cy.getStartingChannel().then(({ slug }) => (channelSlug = slug));

      cy.onBack().then(() => {
        cy.log('Close Guide');
      });

      // cy.location('pathname')
      //   .should('not.eq', LiveTVRoutes.guide)
      //   /** TypeError: Expected "slug" to be defined */
      //   .and('eq', generatePath(LiveTVRoutes.watch, { slug: channelSlug }));
      cy.location().should((location) => {
        expect(location.pathname).to.not.eq(LiveTVRoutes.guide);
        expect(location.pathname).to.eq(generatePath(LiveTVRoutes.watch, { slug: channelSlug }));
      });

      cy.get('button', { log: false }).first({ log: false }).as('fakeDocument');

      const changeChannel = (direction = 'down') => {
        dPad(direction);

        cy.getState().then((state) => {
          const { id, name, slug } = state.player.content;
          prevChannelSlug = channelSlug;
          // selectPlayerContentSlug(state);

          Cypress.log({
            name: 'channel',
            displayName: 'Channel Change',
            message: [name, `id: ${id}`, `slug: ${slug}`],
            consoleProps: () => ({
              id,
              name,
              slug,
              prevChannelSlug,
              content: state.player.content,
            }),
          });
          channelSlug = state.player.content.slug;
        });

        cy.location().should((location) => {
          expect(location.pathname).to.not.eq(
            generatePath(LiveTVRoutes.watch, { slug: prevChannelSlug }),
          );
          expect(location.pathname).to.eq(generatePath(LiveTVRoutes.watch, { slug: channelSlug }));
        });
      };

      changeChannel('up');
      changeChannel('up');
      changeChannel('down');
      changeChannel('down');
      changeChannel('down');
    });

    /** @see https://plutotv.testrail.net/index.php?/cases/view/49255 */
    it('Channel Rocker +/-', () => {
      cy.location('pathname').should('eq', LiveTVRoutes.guide);

      cy.getState().then((state) => {
        const { id, name, slug } = state.player.content;
        // selectPlayerContentSlug(state);

        Cypress.log({
          name: 'channel',
          displayName: 'Starting Channel',
          message: [name, `id: ${id}`, `slug: ${slug}`],
          consoleProps: () => ({
            id,
            name,
            slug,
            content: state.player.content,
          }),
        });
        channelSlug = state.player.content.slug;
      });

      // cy.dispatch(userInput.actions.keyPress({ code: 'onBack' })); // { type: '@@userInput/USER_INPUT/KEY_PRESS', payload: { code: 'onBack' } }
      cy.get('body', { log: false })
        .as('document')
        .type('{esc}', { log: false }) // {backspace}
        .then(() => {
          cy.log('Close Guide');
        });

      cy.location().should((location) => {
        expect(location.pathname).to.not.eq(LiveTVRoutes.guide);
        expect(location.pathname).to.eq(generatePath(LiveTVRoutes.watch, { slug: channelSlug }));
      });

      cy.get('button', { log: false }).first({ log: false }).as('fakeDocument');

      const changeChannel = (direction = 'down') => {
        rocker(direction);
        cy.getState().then((state) => {
          const { id, name, slug } = state.player.content;
          prevChannelSlug = channelSlug;
          // selectPlayerContentSlug(state);

          Cypress.log({
            name: 'channel',
            displayName: 'Channel Change',
            message: [name, `id: ${id}`, `slug: ${slug}`],
            consoleProps: () => ({
              id,
              name,
              slug,
              prevChannelSlug,
              content: state.player.content,
            }),
          });
          channelSlug = state.player.content.slug;
        });

        cy.location().should((location) => {
          expect(location.pathname).to.not.eq(
            generatePath(LiveTVRoutes.watch, { slug: prevChannelSlug }),
          );
          expect(location.pathname).to.eq(generatePath(LiveTVRoutes.watch, { slug: channelSlug }));
        });
      };

      changeChannel('down');
      changeChannel('down');
      changeChannel('up');
      changeChannel('up');
      changeChannel('up');
    });
  });

  /** @see https://plutotv.testrail.net/index.php?/cases/view/14403 */
  describe('Guide', () => {
    it('D-PAD UP/DOWN', () => {
      openEPG();

      // don't go up first as EPG does not wrap, it will select L1
      dPad('down');
      cy.getState().then((state) => {
        // liveTVEPGSelectedIndexSelector(state);

        Cypress.log({
          name: 'selectedIndex',
          displayName: 'EPG Selected Index',
          message: state.liveTV.epg.selectedIndex,
          consoleProps: () => ({
            epg: state.liveTV.epg,
          }),
        });

        expect(state.liveTV.epg.selectedIndex).to.eq(2);
      });
      // ChannelList > a.ChannelItem__selected > ChannelItem__subtitle
      // ChannelList > a.ChannelItem__selected > ChannelItem__description

      dPad('down');
      cy.getState().then((state) => {
        // liveTVEPGSelectedIndexSelector(state);

        Cypress.log({
          name: 'selectedIndex',
          displayName: 'EPG Selected Index',
          message: state.liveTV.epg.selectedIndex,
          consoleProps: () => ({
            epg: state.liveTV.epg,
          }),
        });
        expect(state.liveTV.epg.selectedIndex).to.eq(3);
      });

      dPad('up');
      cy.getState().then((state) => {
        // liveTVEPGSelectedIndexSelector(state);

        Cypress.log({
          name: 'selectedIndex',
          displayName: 'EPG Selected Index',
          message: state.liveTV.epg.selectedIndex,
          consoleProps: () => ({
            epg: state.liveTV.epg,
          }),
        });
        expect(state.liveTV.epg.selectedIndex).to.eq(2);
      });

      dPad('up');
      cy.getState().then((state) => {
        // liveTVEPGSelectedIndexSelector(state);

        Cypress.log({
          name: 'selectedIndex',
          displayName: 'EPG Selected Index',
          message: state.liveTV.epg.selectedIndex,
          consoleProps: () => ({
            epg: state.liveTV.epg,
          }),
        });
        expect(state.liveTV.epg.selectedIndex).to.eq(1);
      });

      dPad('down');
      cy.getState().then((state) => {
        // liveTVEPGSelectedIndexSelector(state);

        Cypress.log({
          name: 'selectedIndex',
          displayName: 'EPG Selected Index',
          message: state.liveTV.epg.selectedIndex,
          consoleProps: () => ({
            epg: state.liveTV.epg,
          }),
        });
        expect(state.liveTV.epg.selectedIndex).to.eq(2);
      });

      dPad('down');
      cy.getState().then((state) => {
        // liveTVEPGSelectedIndexSelector(state);

        Cypress.log({
          name: 'selectedIndex',
          displayName: 'EPG Selected Index',
          message: state.liveTV.epg.selectedIndex,
          consoleProps: () => ({
            epg: state.liveTV.epg,
          }),
        });
        expect(state.liveTV.epg.selectedIndex).to.eq(3);
      });

      // cy.dispatch(userInput.actions.keyPress({ code: 'onOk' })); // { type: '@@userInput/USER_INPUT/KEY_PRESS', payload: { code: 'onOk' } }
      cy.get('body', { log: false }).as('document').type('{enter}', { log: false });
      // cy.pause();

      cy.getState().then((state) => {
        cy.location().should((location) => {
          expect(location.pathname).to.not.eq(LiveTVRoutes.guide);
          expect(location.pathname).to.eq(
            generatePath(LiveTVRoutes.watch, {
              slug: state.player.defaultContent.linearChannel.slug,
            }),
          );
        });
      });
    });

    it('Channel Rocker +/-', () => {
      openEPG();

      // don't go up first as EPG does not wrap, it will select L1
      rocker('down');
      rocker('down');
      rocker('down');
      rocker('up');
      rocker('up');

      // cy.dispatch(userInput.actions.keyPress({ code: 'onOk' })); // { type: '@@userInput/USER_INPUT/KEY_PRESS', payload: { code: 'onOk' } }
      cy.get('body', { log: false }).as('document').type('{enter}', { log: false });
      // cy.pause();

      cy.getState().then((state) => {
        cy.location().should((location) => {
          expect(location.pathname).to.not.eq(LiveTVRoutes.guide);
          expect(location.pathname).to.eq(
            generatePath(LiveTVRoutes.watch, {
              slug: state.player.defaultContent.linearChannel.slug,
            }),
          );
        });
      });
    });
  });
});
