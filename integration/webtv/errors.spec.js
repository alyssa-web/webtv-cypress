/// <reference types="cypress" />

context('App Errors', () => {
  Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false;
  });

  beforeEach(() => cy.visit('/'));

  // beforeEach(() => {
  //   cy.initStore();
  //   cy.bypassPrivacyPolicy();
  // });

  describe('Network Error', () => {
    beforeEach(() => {
      // cy.intercept('master.m3u8', (req) => {
      //   req.reply({
      //     statusCode: 404,
      //     // body: { error: '' },
      //     headers: { 'access-control-allow-origin': '*' },
      //     delayMs: 500,
      //   });
      // }).as('master');
      // cy.intercept('playlist.m3u8', (req) => {
      //   req.reply({
      //     statusCode: 404,
      //     // body: { error: '' },
      //     headers: { 'access-control-allow-origin': '*' },
      //     delayMs: 500,
      //   });
      // }).as('playlist');
      // cy.intercept('.ts', (req) => {
      //   req.reply({
      //     statusCode: 404,
      //     // body: { error: '' },
      //     headers: { 'access-control-allow-origin': '*' },
      //     delayMs: 500,
      //   });
      // }).as('segment');
    });

    it.skip('displays a message', () => {
      cy.pause();
    });
  });

  describe('Chunk Load Error', () => {
    it('displays a message', () => {
      cy.get('#plutotv-app')
        .as('app')
        .find('[data-testid="error-boundary"]')
        .as('guide')
        .should('be.visible');
      // cy.pause();
    });
    it('sends to Loggly');
    it('sends to Phoenix');
  });
});
