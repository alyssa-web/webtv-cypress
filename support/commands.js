/** @see https://on.cypress.io/custom-commands */

import { STORAGE_KEY } from '../../src/modules/privacy-policy/PrivacyPolicy.constants';

/** Get redux store */
Cypress.Commands.add('getStore', () => cy.window({ log: false }).its('store', { log: false }));

/** Get state */
Cypress.Commands.add('getState', () =>
  cy
    .getStore()
    .invoke({ log: false }, 'getState')
    .then((state) => {
      Cypress.log({
        name: 'store',
        message: 'getState()',
        consoleProps: () => ({
          ...state,
        }),
      });

      return state;
    }),
);

/** dispatch */
Cypress.Commands.add('dispatch', (action) => cy.getStore().invoke('dispatch', action));

/** both `cy.initStore();` and `cy.bypassPrivacyPolicy();` are needed to bypass Privacy Policy */
Cypress.Commands.add('initStore', (url = '/') => {
  cy.fixture('store').then((store) => {
    cy.visit(url, {
      onBeforeLoad: (win) => {
        win.initialState = store;
      },
    });
  });
});

/** disable Privacy Policy */
Cypress.Commands.add('bypassPrivacyPolicy', () => {
  cy.fixture('store').then((store) => {
    // isNewPrivacyPolicyVersionSelector
    localStorage.setItem(STORAGE_KEY, store.features.privacyPolicyVersion);
  });
});

Cypress.Commands.add('onBack', () => {
  // cy.dispatch(userInput.actions.keyPress({ code: 'onBack' })); // { type: '@@userInput/USER_INPUT/KEY_PRESS', payload: { code: 'onBack' } }
  cy.get('body', { log: false }).as('document').type('{esc}', { log: false }); // {backspace}
});

Cypress.Commands.add('getStartingChannel', () => {
  cy.getState().then((state) => {
    const { id, name, slug } = state.player.content;
    // selectPlayerContentSlug(state);

    Cypress.log({
      name: 'channel',
      displayName: 'Starting Channel',
      message: [name, `id: ${id}`, `slug: ${slug}`],
      consoleProps: () => ({
        id,
        name,
        slug,
        content: state.player.content,
      }),
    });

    return {
      id,
      name,
      slug,
    };
  });
});

//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
