// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

import 'cypress-wait-until';
import 'cypress-pipe';

import './commands';

Cypress.Server.defaults({
  ignore: (xhr) => {
    // this function receives the xhr object in question and
    // will ignore if it's a GET that appears to be a static resource
    return (
      xhr.method === 'GET' &&
      (/\.(jsx?|coffee|html|less|s?css|svg)(\?.*)?$/.test(xhr.url) || /(sockjs-node)/.test(xhr.url))
    );
  },
});

beforeEach(() => {
  cy.fixture('CONFIG.json')
    .as('CONFIG')
    .then((CONFIG) => {
      Cypress.log({
        name: 'config',
        displayName: 'CONFIG',
        message: 'options',
        consoleProps: () => ({
          ...CONFIG,
        }),
      });
    });
});
